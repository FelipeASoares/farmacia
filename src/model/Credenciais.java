package model;

import java.io.Serializable;

public class Credenciais implements Serializable {

	private static final long serialVersionUID = 2231633173690741400L;

	private String email;

	private String senha;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
