package beans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dao.PessoaDao;
import model.Pessoa;

@ManagedBean(name = "Cadastro")
@RequestScoped
public class CadastroBean implements Serializable {

	private static final long serialVersionUID = -8842800878925920557L;
	private String nome;
	private String sexo;
	private String email;
	private String endereco;
	private Integer idade;
	private String search;
	private List<Pessoa> pessoas;
	private boolean printTable = false;

	public String doSave() {
		try {
			Pessoa pessoa = new Pessoa();
			pessoa.setEmail(email);
			pessoa.setEndereco(endereco);
			pessoa.setIdade(idade);
			pessoa.setNome(nome);
			pessoa.setSexo(sexo);

			PessoaDao p = new PessoaDao();
			p.adicionar(pessoa);
			
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Cadastrado com sucesso!", ""));
			return "";
		} catch (SQLException s) {
			if (s.getErrorCode() == 1062) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email já cadastrado", ""));
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao cadastrar", ""));
			}
			return "";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ""));
			return "";
		}
	}

	public String doSearch() {
		PessoaDao p = new PessoaDao();
		try {
			this.pessoas = p.pesquisa(search);
			this.printTable = true;
			return "";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ""));
			return "";
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public List<Pessoa> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

	public boolean isPrintTable() {
		return printTable;
	}

	public void setPrintTable(boolean printTable) {
		this.printTable = printTable;
	}

}
