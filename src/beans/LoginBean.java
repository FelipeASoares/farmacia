package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import dao.Credencial;
import model.User;
import session.SessionContext;

@ManagedBean(name = "LoginBean")
@SessionScoped
public class LoginBean {

	private String email;
	private String password;
	private String nome;

	public User getUser() {
		return (User) SessionContext.getInstance().getUsuarioLogado();
	}

	public String doLogin() {
		try {
			if (email.equals("") || password.equals("")) {
				return "";
			} else {
				Credencial check = new Credencial();
				User usuario = check.Login(email, password);
				this.nome = usuario.getNome();
				System.out.println(usuario);
				SessionContext.getInstance().setAttribute("usuarioLogado", usuario);
				return "/auth/cadastro.xhtml?faces-redirect=true";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public String doLogout() {
		SessionContext.getInstance().encerrarSessao();
		return "/login.xhtml?faces-redirect=true";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
