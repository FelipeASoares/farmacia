package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Pessoa;

public class PessoaDao {

	public int adicionar(Pessoa pessoa) throws Exception {
		PreparedStatement ps = null;
		int codigoRetorno = 0;
		try (Connection conn = (Connection) new FabricaDeConexoes().getConnection()) {
			ps = (PreparedStatement) conn.prepareStatement(
					"insert into pessoa(nome, sexo, email, endereco, idade)" + " values(?, ?, ?, ?, ?)");
			ps.setString(1, pessoa.getNome());
			ps.setString(2, pessoa.getSexo());
			ps.setString(3, pessoa.getEmail());
			ps.setString(4, pessoa.getEndereco());
			ps.setInt(5, pessoa.getIdade());

			codigoRetorno = ps.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					throw new SQLException(e);
				}
			}
		}
		return codigoRetorno;
	}

	public ArrayList<Pessoa> pesquisa(String pesquisa) throws Exception {
		PreparedStatement ps = null;
		ArrayList<Pessoa> retorno = null;
		try (Connection conn = (Connection) new FabricaDeConexoes().getConnection()) {
			ps = (PreparedStatement) conn.prepareStatement(
					"select nome, sexo, email, endereco, idade from pessoa" + " where nome like ? or email like ?");
			ps.setString(1, "%" + pesquisa + "%");
			ps.setString(2, "%" + pesquisa + "%");

			ResultSet query = ps.executeQuery();
			retorno = new ArrayList<>();
			while (query.next()) {
				Pessoa p = new Pessoa();
				p.setEmail(query.getString("email"));
				p.setNome(query.getString("nome"));
				p.setSexo(query.getString("sexo"));
				p.setEndereco(query.getString("endereco"));
				p.setIdade(query.getInt("idade"));
				
				retorno.add(p);
			}
			
		} catch (SQLException e) {
			throw e;
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					throw e;
				}
			}
		}
		return retorno;
	}
}
