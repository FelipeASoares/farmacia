package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public class Credencial {

	public User Login(String email, String senha) throws Exception {
		User retorno = null;
		PreparedStatement ps = null;
		try (Connection conn = (Connection) new FabricaDeConexoes().getConnection()) {
			ps = (PreparedStatement) conn
					.prepareStatement("Select * from user where email = ? and password = ?");
			ps.setString(1, email);
			ps.setString(2, senha);

			ResultSet resultadoQuery = ps.executeQuery();
			retorno = new User();
			while(resultadoQuery.next()) {
				retorno.setEmail(resultadoQuery.getString("email"));
				retorno.setNome(resultadoQuery.getString("nome"));
			}

		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					throw new SQLException(e);
				}
			}
		}
		return retorno;
	}
}
