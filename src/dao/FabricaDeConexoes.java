package dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class FabricaDeConexoes {

	public Connection getConnection() throws Exception {
		String url = "jdbc:mysql://localhost/pessoas";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return (Connection) DriverManager.getConnection(url, "suasCredenciais", "suasCredenciais");
		} catch (Exception e){
			throw new Exception(e);
		}
	}
}